const express = require("express");
const bodyParser = require("body-parser");
const redis = require("redis");
const connection = require("./db").getInstance;
var watcher = null;
//express APP
const app = express();
var isConnected = false;
const conn = function() {
  connection()
    .authenticate()
    .then(function() {
      console.log("Connected to database");
      isConnected = true;
    })
    .catch(err => {
      console.log("Connection Error", err.message);
      isConnected = false;
    });
};
conn();

app.use(bodyParser.json());
//config App
let port = 3300;

// redis Client
const client = redis.createClient();

client.on("connect", function() {
  console.log("Connected to Redis Server ... ");
});

// Routes
app.get("/", function(req, res, next) {
  res.status(200).json({
    message: "OK"
  });
});

app.post("/message", function(req, res, next) {
  console.log(req.body);

  res.status(200).json({
    message: "OK"
  });

  let id = req.body.STAN;
  if (client.hmset("khizar", [id, JSON.stringify(req.body)])) {
    console.log("Value Saved");
  } else {
    console.log("Request not saved into Redis : ", JSON.stringify(req.body));
    return false;
  }

  checkDbConnection()
    .then(data => {
      console.log("connection checked");
    })
    .then(data => {
      client.hgetall("khizar", function(...args) {
        // console.log("args... ", args);
        JsonParsing(args);
      });
    });
});

function JsonParsing(jsonObj) {
  console.log("Now Parsing JSON");
  Object.values(jsonObj).forEach(element => {
    if (element != null) {
      //   console.log("element", element);
      console.log("==========");
      Object.values(element).forEach(tupple => {
        // console.log("tupple", JSON.parse(tupple));
        let objKey = JSON.parse(tupple).STAN.toString();
        insertRequestToDB(JSON.parse(tupple))
          .then(data => {
            client.HDEL("khizar", objKey);
          })
          .catch(err => {
            console.log("The record didnt save into DB");
          });
      });
    }
  });
}

function insertRequestToDB(recordToInsert) {
  console.log("Now inside insertIntoDB method ");
  let {
    STAN,
    ANI,
    relationship_no,
    transaction_date_time,
    channel_type,
    response_code,
    transactionListId
  } = recordToInsert;

  return new Promise((resolve, reject) => {
    var sql = `INSERT INTO tranlogs (STAN,ANI,relationship_no,transaction_date_time,channel_type,response_code,transactionListId) VALUES   ('${STAN}','${ANI}','${relationship_no}','${transaction_date_time}','${channel_type}','${response_code}','${transactionListId}')`;
    connection()
      .query(sql, function(err, result) {
        console.log("1 record inserted");
      })
      .then(data => {
        resolve(true);
      })
      .catch(err => {
        console.log("Error ocuured ", err.message);
        reject(false);
      });
  });
}

function checkDbConnection() {
  return new Promise((resolve, reject) => {
    if (!watcher)
      watcher = setInterval(function() {
        console.log("conn", isConnected);
        if (!isConnected) {
          conn();
          console.log("Trying to connect DB ...");
        } else {
          clearInterval(watcher);
          console.log("Connected to DB ...");
          isConnected = true;
          resolve(true);
        }
        isConnected = false;
      }, 3000);
  });
}

// listening Port
app.listen(port, function() {
  console.log("Listening to the port : " + port);
});
