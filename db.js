const Sequelize = require("sequelize");
let instance;

module.exports = (function() {
  function createInstance() {
    return new Sequelize("Chthonic", "khizar", "password", {
      host: "127.0.0.1",
      dialect: "mssql",
      logging: false,
      operatorsAliases: false,
      dialectOptions: {
        encrypt: false
      },
      pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
    });
  }

  return {
    getInstance: function() {
      if (!instance) {
        instance = createInstance();
      }
      return instance;
    }
  };
})();
